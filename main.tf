provider "aws" {
  region = "ap-south-1" 
}

variable "cidr_blocks" {
  description = "CIDR blocks for VPC and subnets"
  type = list(string)
}
variable "subnets" {
  description = "subnets will be created in VPC"
  type = list(object({
    cidr_block = string
    name = string
  }))
}
resource "aws_vpc" "test-vpc" {
  cidr_block = var.cidr_blocks[0]
  tags = {
    Name : "testVPC_TF"
  }
}

variable avail_zone {}

variable "subnet_block"{
  description = "subner CIDR"
  default = "10.0.20.0/24"
}
resource "aws_subnet" "test-subnet03" {
  vpc_id = aws_vpc.test-vpc.id
  cidr_block = var.subnet_block
  availability_zone = var.avail_zone
  tags = {
    Name : "testSubnet3"
  }
}

resource "aws_subnet" "test-subnet05" {
  vpc_id = aws_vpc.test-vpc.id
  cidr_block = var.subnets[0].cidr_block
  availability_zone = "ap-south-1a"
  tags = {
    Name : var.subnets[0].name
  }
}

resource "aws_subnet" "test-subnet01" {
  vpc_id = aws_vpc.test-vpc.id
  cidr_block = var.cidr_blocks[1]
  availability_zone = "ap-south-1a"
  tags = {
    Name : "testSubnet1"
  }
}

#variable "vpc_id" {}

data "aws_vpc" "existing_vpc"{
  id = "vpc-0fc46188d95958d8a"
}

resource "aws_subnet" "test-subnet02" {
  vpc_id = data.aws_vpc.existing_vpc.id
  cidr_block = cidrsubnet(data.aws_vpc.existing_vpc.cidr_block, 4, 1)
  availability_zone = "ap-south-1b"
  tags = {
    Name : "testSubnet2"
  }
}

output "testVpcID" {
  value = aws_vpc.test-vpc.id
}

output "testSubnet01ID" {
  value = aws_subnet.test-subnet01.id
}
